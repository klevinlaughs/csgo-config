# TODO: Check if files exist already

# TODO: file picker?
Function Get-Folder($initialDirectory = "") {
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null

    $foldername = New-Object System.Windows.Forms.FolderBrowserDialog
    $foldername.Description = "Select a folder"
    $foldername.rootfolder = "MyComputer"
    $foldername.SelectedPath = $initialDirectory

    if ($foldername.ShowDialog() -eq "OK") {
        $folder += $foldername.SelectedPath
    }
    return $folder
}

# BananaGaming
# csgo_bananagaming.txt => csgo/resource
# radiopanel.txt => csgo/resource/ui

# CFG
# all to csgo/cfg