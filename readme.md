# Kelvin's csgo config :D

## CSGO launch options

Set launch options in steam:

```
+fps_max 0 +cl_forcepreload 1 -novid -nojoy -high -language bananagaming

// maybe needs "+exec autoexec.cfg" as well
```

If specific resolution is needed, add `-full -w <width> -h <height>`

## cfg files

Copy or symlink cfg's into `<csgo-location>/csgo/cfg`.

## Other things

- [Banana Gaming](https://bananagaming.tv/)
  - radio panel (slightly customised, in `<csgo-location>/csgo/resource/ui`)
  - text color mod (in `<csgo-location>/csgo/resource`)
  - NB: radio panel has trajectory cfg scripts in there
- [VibranceGUI](https://vibrancegui.com/)

## TODO

- [ ] Write script to symlink

```sh
csgodir=~/.steam/steam/steamapps/common/Counter-Strike\ Global\ Offensive
# -f flag to force and remove existing destination
# -b flag for backup
ln -sb cfg/*.cfg "$csgodir/csgo/cfg/"

ln -sb bananagaming/radiopanel.txt "$csgodir/csgo/resource/ui"

# -b flag also backup
cp -b bananagaming/csgo_bananagaming.txt "$csgodir/csgo/resource/"
```
